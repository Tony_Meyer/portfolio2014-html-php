/* 

TO DO

1) Reduce CSS duplication
   - Ideally just a single build - global.scss turns into /build/global.css
   - Can Autoprefixer output minified?
   - If it can, is it as good as cssmin?
   - Could Sass be used again to minify instead?
   - If it can, is it as good as cssmin?

2) Better JS dependency management
   - Require js?
   - Can it be like the Asset Pipeline where you just do //= require "whatever.js"

3) Is HTML minification worth it?

4) Set up a Jasmine test just to try it.

5) Can this Gruntfile.js be abstracted into smaller parts?
   - https://github.com/cowboy/wesbos/commit/5a2980a7818957cbaeedcd7552af9ce54e05e3fb

*/    

module.exports = function(grunt) {

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    sass: {
      dist: {
        options: {
          // cssmin will minify later
          style: 'expanded'
        },
        files: {
          'samsonite/themes/portfolio2014/ncss/base.css': 'samsonite/themes/portfolio2014/sass/sbase.scss'
        }
      }
    },

    autoprefixer: {
      options: {
        browsers: ['last 2 version']
      },
      multiple_files: {
        expand: true,
        flatten: true,
        src: 'samsonite/themes/portfolio2014/ncss/*.css',
        dest: 'samsonite/themes/portfolio2014/ncss/prefixed/'
      }
    },

    cssmin: {
      combine: {
        files: {
          'samsonite/themes/portfolio2014/ncss/base.min.css': ['samsonite/themes/portfolio2014/ncss/prefixed/base.css']
        }
      }
    },

    jshint: {
      beforeconcat: ['js/*.js']
    },

    concat: {
      dist: {
        src: [
          'samsonite/themes/portfolio2014/js/libs/jquery.js',
          'samsonite/themes/portfolio2014/js/libs/modernizr262.js',
          'samsonite/themes/portfolio2014/js/libs/plugins.js',
          'samsonite/themes/portfolio2014/js/dev-app.js'
        ],
        dest: 'samsonite/themes/portfolio2014/js/production/production.js'
      }
    },

    uglify: {
      build: {
        src: 'samsonite/themes/portfolio2014/js/production/production.js',
        dest: 'samsonite/themes/portfolio2014/js/production/production.min.js'
      }
    },

    imagemin: {
      dynamic: {
        files: [{
          expand: true,
          cwd: 'samsonite/themes/portfolio2014/img/',
          src: ['**/*.{png,jpg,gif}'],
          dest: 'samsonite/themes/portfolio2014/img/'
        }]
      }
    },

    watch: {
      options: {
        livereload: true,
      },
      scripts: {
        files: ['js/*.js'],
        tasks: ['concat', 'uglify', 'jshint'],
        options: {
          spawn: false,
        }
      },
      css: {
        files: ['css/*.scss'],
        tasks: ['sass', 'autoprefixer', 'cssmin'],
        options: {
          spawn: false,
        }
      },
      images: {
        files: ['samsonite/themes/portfolio2014/img/**/*.{png,jpg,gif}', 'samsonite/themes/portfolio2014/img/*.{png,jpg,gif}'],
        tasks: ['imagemin'],
        options: {
          spawn: false,
        }
      }
    },

    connect: {
      server: {
        options: {
          port: 8000,
          base: './'
        }
      }
    },

  });

  require('load-grunt-tasks')(grunt);

  // Default Task is basically a rebuild
  grunt.registerTask('default', ['concat', 'uglify', 'sass', 'autoprefixer', 'cssmin', 'imagemin']);

  grunt.registerTask('dev', ['connect', 'watch']);

};