<?php  
     
DEFINE(PRODUCTION, FALSE);
DEFINE(BUSTER, "?v=1.0");

if(PRODUCTION) {
	//DEFINE(PATHROOT, "http://tonymeyer.org"."/");
	//DEFINE(DIR, "2014"."/");
	DEFINE(SCRIPTSJS, "scripts.js");
	DEFINE(APPJS, "app.js");
}

DEFINE(SCRIPTSJS, "dev-scripts.js");
DEFINE(APPJS, "dev-app.js");

DEFINE(PATHROOT, "/");
DEFINE(DIR, "2014"."/");
DEFINE(ROOT, $_SERVER['DOCUMENT_ROOT']);

defined("PARTS")  
    or define("PARTS", realpath(dirname(__FILE__) . '/parts'));
  
?> 