<?php 
    require_once("../_config.php");  
    require_once(PARTS . "/_header.php");  
?>

	<section class="pTop120"></section>
	<section id="page-title" class="container">
		<div class="row">	
			<div class="span10">
				<h2>Buzz</h2>
			</div>
			<div class="span2 hidden-phone">
				<p><span class="number">Loading..</span>posts</p>

			</div>
		</div>
		<div class="row hidden-phone">
			<hr class="span12 clear-bottom">
		</div>
	</section>
	<!-- End Title -->
					
	<!-- Start Blog Post -->			
<span id="render"><div id="spinner"></div></span>
	<!-- End Blog Post -->

<?php require_once(PARTS . "/_footer.php");  ?>