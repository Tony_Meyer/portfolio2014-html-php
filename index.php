<?php 
    require_once("_config.php");  
    require_once(PARTS . "/_header.php");  
?>
	<section class="pTop120"></section>
	<!-- End Header --><!-- Start Hero Carousel -->
	<section class="hero">
		<div class="heroImg">
			<article>
				<img class=""  src="samsonite/themes/portfolio2014/img/hero-cat.jpeg" alt="superman" />
				<div class="contents">
					<h4 class="color-text"></h4>
					<p></p>
				</div>
			</article>
		</div>
	</section>
	<!-- End Hero Carousel -->
		<!-- Start Intro Block -->
	<section class="intro container">
		<div class="row">
			<div class="span12">
				<p>Hi, I'm a web developer and I build applications and websites using emerging web technologies. My preferred tools for providing solutions are JavaScript, PHP, HMTL5 and CSS3 <br /> <a href="<?php echo(PATHROOT.DIR); ?>work/">portfolio</a> | <a href="<?php echo(PATHROOT.DIR); ?>timeline/">resume</a></p>
			</div>
		</div>
	</section>
	<!-- End Intro Block -->	
<!-- Start Services Icons -->
	<section id="services" class="container">
		<div class="row">
			<div class="span6">
				<div class="service-icon icon1"></div>
				<div class="service-desc">
					<h5>Digital Engineering</h5>
					<p>Hand me a layout concept designed in Photoshop, Illustrator, MSPaint or napkin doodle, and I will bring it to life on the web by coding it into a set of template files and preparing it for content management if desired. Every pixel(doodle) counts!</p>
				</div>
			</div>
			<div class="span6">
				<div class="service-icon icon4"></div>
				<div class="service-desc">
					<h5>Design</h5>
					<p>Being well versed with graphic design software like Photoshop, Illustrator is of great value for good web design, while this is not part my core skill set, I am proficient with this software and have been using it for over 10 years and still use it on a daily basis.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="span6">
				<div class="service-icon icon3"></div>
				<div class="service-desc">
					<h5>Content Management</h5>
					<p>Content management systems (CMS) are very prevalent today, I am familiar with and have used several open source and proprietary CMS systems including Typo3, WordPress, ProcessWire, Pyro CMS, FirstSpirit, Drupal and more.</p>
				</div>
			</div>
			<div class="span6">
				<div class="service-icon icon2"></div>
				<div class="service-desc">
					<h5>Eternal Student</h5>
					<p>Web development is undergoing rapid change and is constantly growing as new technology, software and standards advance. I am a constant student learning and absorbing all new developments affecting the tools, browsers and code in the online world. </p>
				</div>
			</div>
		</div>
	</section>
	
<?php require_once(PARTS . "/_footer.php");  ?>