	</div> <!-- end class="ajaxContainer" -->
	<!-- Start Social Panel -->
	<section id="social-data">
		<div class="container">
			<div class="row">
				<div class="span4">
					<h5>Contact</h5>
					<address>
						<strong>E-mail:</strong> <a href="#">mediamaker@gmail.com</a>						
					</address>
					<div class="follow">
						<p>social profiles:</p>
						<ul class="social-icons">
							<li><a href="https://github.com/thm-design" target="_blank" title="Github"><span><i class="icon-github"></i></span></a></li>
							<li><a href="https://coderbits.com/Design4life" target="_blank" title="Coderbits"><span><i class="icon-beaker"></i></span></a></li>
							<li><a href="http://www.linkedin.com/pub/tony-meyer/14/7b7/737" target="_blank" title="Linkedin"><span><i class="icon-linkedin"></i></span></a></li>
							<li><a href="https://twitter.com/design4lifeblog" target="_blank"><span><i class="icon-twitter" title="Twitter"></i></span></a></li>
							<li><a href="https://plus.google.com/u/0/b/114004035745729067811/+TonymeyerOrg/posts" target="_blank" title="Google Plus"><span><i class="icon-google-plus"></i></span></a></li>
							<li><a href="https://vimeo.com/user2584000" target="_blank" title="Vimeo"><span><i class="icon-vimeo"></i></span></a></li>
						</ul>
					</div>
					<hr class="visible-phone">
				</div>
				<div class="span4">
					<h5>Latest tweets <a class="hidden-tablet" href="http://twitter.com/design4lifeblog" target="_blank">@design4lifeblog</a></h5>
					<div id="twitter-feed"></div>
					<hr class="visible-phone">
				</div>
				<div class="span4">
					<h5>Photostream</h5>
					<ul id="basicuse" class="photostream thumbs unstyled"></ul>
				</div>
			</div>
		</div>
	</section>
	<!-- End Services Icons -->
	
	<!-- Start Clients Block -->
	<div id="clients" class="clients container hidden-phone">
		<div class="row">
			<div class="span12">
				<div class="vertical-align">
					<ul class="unstyled">
						<li><a href="#"><img src="<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/img/clients/logo1.png" alt="" /></a></li>
						<li><a href="#"><img src="<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/img/clients/logo2.png" alt="" /></a></li>
						<li><a href="#"><img src="<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/img/clients/logo3.png" alt="" /></a></li>
						<li><a href="#"><img src="<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/img/clients/logo4.png" alt="" /></a></li>
						<li><a href="#"><img src="<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/img/clients/logo5.png" alt="" /></a></li>
						<li><a href="#"><img src="<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/img/clients/logo6.png" alt="" /></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- End Clients Block -->

	<!-- Start Footer -->
	<footer class="container">
		<div class="row">
			<div class="span12">
				<!-- Start Footer Navigation -->
				<nav class="hidden-phone">
				<ul id="navigation" class="pull-right hidden-phone">
				<li><a href="<?php echo(PATHROOT.DIR); ?>">Home</a></li>
				<li><a href="<?php echo(PATHROOT.DIR); ?>work/">Portfolio</a></li>
				<li><a href="<?php echo(PATHROOT.DIR); ?>skills/">Skills</a></li>
				<li><a href="<?php echo(PATHROOT.DIR); ?>timeline/">Resume</a></li>
				<li><a href="<?php echo(PATHROOT.DIR); ?>buzz/">Buzz</a></li>
				</ul>		
				</nav>
				<!-- End Footer Navigation -->
				<!-- Copyright -->
				<p class="copyright">
					&copy; 2013 <a href="<?php echo(PATHROOT.DIR); ?>" title="<?php echo(PATHROOT.DIR); ?>">tony h. meyer</a>
				</p>
				<!-- /Copyright -->		
			</div>
		</div>
	</footer>
	<!-- End Footer -->	
	
	<script>
		var _gaq=[['_setAccount','UA-15207111-11'],['_trackPageview']];
		var pluginUrl = 
		 '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
		_gaq.push(['_require', 'inpage_linkid', pluginUrl]);
		(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
		g.src='//www.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g,s)})(document,'script');
	</script>

	</div><!--inner-->
</div><!--outer-->
<script type='text/javascript' src='<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/js/production/production.min.js'></script>
<?php 
		if($_SERVER['REQUEST_URI'] == PATHROOT.DIR."skills"."/" ){
			echo "<script type='text/javascript' src='".PATHROOT.DIR."samsonite/themes/portfolio2014/js/vendor/chart.js'></script>";
			echo "<script type='text/javascript' src='".PATHROOT.DIR."samsonite/themes/portfolio2014/js/vendor/chartscript.js'></script>";
		}
		if($_SERVER['REQUEST_URI'] == PATHROOT.DIR."work"."/" ){
			echo "<script type='text/javascript' src='".PATHROOT.DIR."samsonite/themes/portfolio2014/js/vendor/jquery.isotope.min.js'></script>";
		}
?>
</body>
</html>