<!doctype html>
<html lang="en-US" class="no-js" lang="en">
	<head>
		<meta charset="UTF-8">
		<!-- DNS Prefetch -->
		<link rel="dns-prefetch" href="http://www.google-analytics.com">
		<link rel="dns-prefetch" href="http://cdn.syndication.twimg.com"/>
		
		<title>tony h. meyer </title>
		
		<!-- Meta -->
		<meta name="HandheldFriendly" content="true">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="description" content="">	
		<!-- icons -->
		<link rel="shortcut icon" href="<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/apple-touch-icon.png" />
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/apple-touch-icon-57x57.png" />
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/apple-touch-icon-72x72.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/apple-touch-icon-114x114.png" />
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/apple-touch-icon-144x144.png" />
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/apple-touch-icon-60x60.png" />
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/apple-touch-icon-120x120.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/apple-touch-icon-76x76.png" />
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/apple-touch-icon-152x152.png" />
			
		<!-- css + javascript -->
		<meta name='robots' content='noindex,nofollow' />
		<link rel='stylesheet' id='base-css'  href='<?php echo(PATHROOT.DIR); ?>samsonite/themes/portfolio2014/ncss/base.min.css' media='all' />
		<?php 
				if($_SERVER['REQUEST_URI'] == PATHROOT.DIR."timeline"."/" ){
					echo "<link rel='stylesheet 'href='".PATHROOT.DIR."samsonite/themes/portfolio2014/css/component.css' media='all'>";
					echo "<style type='text/css'>
										*, *:after, *:before { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; }
															body, html { font-size: 100%; padding: 0; margin: 0;}
							</style> ";
		}
		?>

		<?php 
		if($_SERVER['REQUEST_URI'] == PATHROOT.DIR."work/basechat"."/" || $_SERVER['REQUEST_URI'] == PATHROOT.DIR."work/octopress"."/"){
			echo "<link rel='stylesheet' id='gitcss-css'  href='".PATHROOT.DIR."samsonite/themes/portfolio2014/css/git.css' media='all' />";
		}
		?>
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->	
	</head>
		<body>
	  <div id="outer-wrap">
    <div id="inner-wrap">
	<!-- Start Header -->
	<header id="top">
		<div class="block container">
			<div class="row">
				<div class="span5">
				<div class="logo">
					<a class="block-title" href='<?php echo(PATHROOT.DIR); ?>'>tony h. meyer</a>
					<a class="nav-btn" id="nav-open-btn" href="#nav">Navigation</a>
				</div>
				</div>
				<div class="span7">
				</div>
			</div>
		</div>
	</header>
	<nav id="nav" role="navigation">
		<div class="block container">
			<div class="logo">
					<a id="newlogo" href="<?php echo(PATHROOT.DIR); ?>">tony h. meyer</a>
				</div>
	  	<h2 class="block-title">Sections</h2>
	  		<ul class="pull-right">
					<li id="hash">
		          <a class="icon icon-shop" href="<?php echo(PATHROOT.DIR); ?>#home">Home</a>
		      </li><!--
		   --><li>
		          <a class="icon icon-lab" href="<?php echo(PATHROOT.DIR); ?>work/">Portfolio</a>
		      </li><!--
		   --><li>
		   				<a class="icon icon-params" href="<?php echo(PATHROOT.DIR); ?>skills/">Skills</a>
		      </li><!--
		   --><li>
		          <a class="icon icon-study" href="<?php echo(PATHROOT.DIR); ?>timeline/">Resume</a>
		      </li><!--

		   --><li>
		   				<a class="icon icon-fire" href="<?php echo(PATHROOT.DIR); ?>buzz/">Buzz</a>
		      </li>
				</ul>
	    <a class="close-btn" id="nav-close-btn" href="#top">Return to Content</a>
		</div>
	</nav> 
	<div class="ajaxContainer">