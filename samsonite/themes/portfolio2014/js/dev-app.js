// init fastclick -- https://github.com/ftlabs/fastclick
$(function() {
    FastClick.attach(document.body);
});

$(function(){
	$('body').ulike({
		icon: 'Heart',
		wording: 'like',
		path: '/2014/ulike/',
	});
});
// attack ajax response div (to say when a item has been liked)
//$('.span4.image-desc').append('<div style="display:none;" class="response alert"></div>');

setTimeout(function() {
	$('.thm-likes').remove();
}, 100);

// if page  NOT == buzz
// ====================================================
if(location.pathname !== "/2014/buzz/") {

	twitterFetcher.fetch('405795008655024128', 'twitter-feed', 2, true);
	$(window).load(function() {
		$('.user').remove();
		$('.interact').remove();
	});

} // End if(location.pathname NOT == "/2014/buzz/") {


// if page  == buzz
// ====================================================
if(location.pathname == "/2014/buzz/") {

	$('#page-title').spin();

	twitterFetcher.fetch('405795008655024128', '', 8, true, true, true, '', false, handleTweets, false);

	function dateFormatter(date) {
	  return date.toTimeString();
	}

	function randInt() {
	    return Math.floor(Math.random() * 90 + 10)
	}

	var getRand = (function() {
	    var nums = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 
									20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 
									30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 
									40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 
									50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 
									60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 
									70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 
									80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 
									90, 91, 92, 93, 94, 95, 96, 97, 98, 99];
	    var current = [];
	    function rand(n) {
	        return (Math.random() * n)|0;
	    }
	    return function() {
	      if (!current.length) current = nums.slice();
	      return current.splice(rand(current.length), 1);
	    }
	}());

	function handleTweets(tweets){

	//console.log(map);

	    var x = tweets.length;
	    var n = 0;
	    var randy = randInt();
	    var element = document.getElementById('render');
	    var html = '<span>';
	    while(n < x) {

	    	//begin tweet parsing
	    	twitt = tweets[n].split(">");
	    	twittstr = twitt.join();
	    	r = twittstr.replace(/,/g, ">,");
	    	commasep = r.split(",");

	   	//console.log(print_r(commasep));  //call it like this

			// function print_r(arr,level) {
			// var dumped_text = "";
			// if(!level) level = 0;

			// //The padding given at the beginning of the line.
			// var level_padding = "";
			// for(var j=0;j<level+1;j++) level_padding += "    ";

			// if(typeof(arr) == 'object') { //Array/Hashes/Objects 
			//     for(var item in arr) {
			//         var value = arr[item];

			//         if(typeof(value) == 'object') { //If it is an array,
			//             dumped_text += level_padding + "'" + item + "' ...\n";
			//             dumped_text += print_r(value,level+1);
			//         } else {
			//             dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
			//         }
			//     }
			// } else { //Stings/Chars/Numbers etc.
			//     dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
			// }
			// return dumped_text;
			// }

			// var showdate = "";
			// if (commasep[28]!== undefined) {
			// 	var showdate = commasep[24]+commasep[25]+commasep[27]+commasep[28];
			// } else {
			// 	var showdate = commasep[25]+commasep[26];
			// } <img id="desaturate" src="../samsonite/themes/portfolio2014/img/lorem/lorem_88.jpeg" alt="">

	      html += '<article class="blog-post container">';
	      	html += '<div class="row">';
	      		html += '<div class="span12">';
	     				html += '<h4>'+tweets[n]+'</h4>';
	      		html += '</div>';
	      	html += '</div>';
	      html += '<div class="row">';
					html += '<div class="span12">';
						html += '<div class="preview-img">';
						html += '<img id="desaturate" src="../samsonite/themes/portfolio2014/img/lorem/lorem_'+ getRand() + '.jpeg" alt="" />';
						html += '</div>';
					html += '</div>';
				html += '</div>';
				html += '<div class="row">';
				html += '<hr class="span12 dotted">';
				html += '</div>';
			html += '</article>';
	      n++;
	    }
	    html += '</span>';
	    element.innerHTML = html;
	    $(".user, .interact").remove();
	}
	$(window).load(function() {
	    var post = $('p.tweet').length;
	    if(post == '0') {
	    	post = 'error';
	    }
	  	//   if($('p.tweet > a:nth-child(2)')){
				// 	$('p.tweet > a:nth-child(2)').remove();
				// }
			$('.number').text(post);


			// Dynamic images loader
			allImagesLoaded = function() {
				var imgs = $('#render').find('img#desaturate');
			    var loaded = 0;
			    imgs.each(function() { 
			    	if ($(this.complete)){ 
			    		++loaded; 
			    		//console.log('all posts loaded');
			    	} else {
			    		//console.log('error');
			    	} 
			    });
			    //setTimeout(function() {
			    if (imgs.length == loaded) {
			    	//console.log('imgs == loaded')
						$('#page-title').spin(false);
						$('#render').show();
						//$('.preview-img').spin();
						setTimeout(function() {
						$('.preview-img').fadeTo( "slow", 1 );
						//$('.preview-img').spin(false);
						}, 300);
			    }
			    //}, 1000);
			}();

			// when user browses to page
			//$('.blog-post img').hide();
			//$('#page-title').spin();

			// then when the #content div has loaded
      $('#render').bind('load', allImagesLoaded);	

			
	});
} // End if pathname == buzz


// if page = work
// ====================================================
if(location.pathname == "/2014/work/") { 
			$(function(){
			$('.post.span4').show();
			var $container = $('#posts');
			$container.isotope({
				itemSelector : '.post'
			});
			var $optionSets = $('#page-title .option-set'),
				$optionLinks = $optionSets.find('a');
			$optionLinks.click(function(){
				var $this = $(this);
				// don't proceed if already selected
				if ( $this.hasClass('selected') ) {
					return false;
				}
				var $optionSet = $this.parents('.option-set');
				$optionSet.find('.selected').removeClass('selected');
				$this.addClass('selected');
			// make option object dynamically, i.e. { filter: '.my-filter-class' }
			var options = {},
				key = $optionSet.attr('data-option-key'),
				value = $this.attr('data-option-value');
				
			// parse 'false' as false boolean
			value = value === 'false' ? false : value;
			options[ key ] = value;
				if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
				// changes in layout modes need extra logic
				changeLayoutMode( $this, options )
			} else {
				// otherwise, apply new options
				$container.isotope( options );
			}    
			return false;
			});
		});
} // End if pathname == work

// active state for nav
// ====================================================
$('div #nav ul a').each(function() {
	var hash = window.location.hash;
    if(window.location.pathname.indexOf($(this).attr('href')) === 0) {
        $(this).parent().addClass('is-active active');
        $(this).addClass('active');
    }
    $('#hash').removeClass('is-active active');
    $('#hash a').removeClass('active');
    if((hash) === "#home"){
			$('#hash').addClass('is-active active');
			$('#hash a').addClass('active');
		} 
});
// Mobile Nav
// ====================================================
/*!
 *
 *  Copyright © David Bushell | BSD & MIT license
 *  https://github.com/dbushell/Responsive-Off-Canvas-Menu
 */
(function(window, document, undefined)
{
    // helper functions
    var trim = function(str){
        return str.trim ? str.trim() : str.replace(/^\s+|\s+$/g,'');
    };

    var hasClass = function(el, cn){
        return (' ' + el.className + ' ').indexOf(' ' + cn + ' ') !== -1;
    };

    var addClass = function(el, cn){
        if (!hasClass(el, cn)) {
            el.className = (el.className === '') ? cn : el.className + ' ' + cn;
        }
    };

    var removeClass = function(el, cn){
        el.className = trim((' ' + el.className + ' ').replace(' ' + cn + ' ', ' '));
    };

    var hasParent = function(el, id){
        if (el) {
            do {
                if (el.id === id) {
                    return true;
                }
                if (el.nodeType === 9) {
                    break;
                }
            }
            while((el = el.parentNode));
        }
        return false;
    };

    // normalize vendor prefixes

    var doc = document.documentElement;

    var transform_prop = window.Modernizr.prefixed('transform'),
        transition_prop = window.Modernizr.prefixed('transition'),
        transition_end = (function() {
            var props = {
                'WebkitTransition' : 'webkitTransitionEnd',
                'MozTransition'    : 'transitionend',
                'OTransition'      : 'oTransitionEnd otransitionend',
                'msTransition'     : 'MSTransitionEnd',
                'transition'       : 'transitionend'
            };
            return props.hasOwnProperty(transition_prop) ? props[transition_prop] : false;
        })();

    window.App = (function()
    {

        var _init = false, app = { };

        var inner = document.getElementById('inner-wrap'),

            nav_open = false,

            nav_class = 'js-nav';


        app.init = function()
        {
            if (_init) {
                return;
            }
            _init = true;

            var closeNavEnd = function(e)
            {
                if (e && e.target === inner) {
                    document.removeEventListener(transition_end, closeNavEnd, false);
                }
                nav_open = false;
            };

            app.closeNav =function()
            {
                if (nav_open) {
                    // close navigation after transition or immediately
                    var duration = (transition_end && transition_prop) ? parseFloat(window.getComputedStyle(inner, '')[transition_prop + 'Duration']) : 0;
                    if (duration > 0) {
                        document.addEventListener(transition_end, closeNavEnd, false);
                    } else {
                        closeNavEnd(null);
                    }
                }
                removeClass(doc, nav_class);
            };

            app.openNav = function()
            {
                if (nav_open) {
                    return;
                }
                addClass(doc, nav_class);
                nav_open = true;
            };

            app.toggleNav = function(e)
            {
                if (nav_open && hasClass(doc, nav_class)) {
                    app.closeNav();
                } else {
                    app.openNav();
                }
                if (e) {
                    e.preventDefault();
                }
            };

            // open nav with main "nav" button
            document.getElementById('nav-open-btn').addEventListener('click', app.toggleNav, false);

            // close nav with main "close" button
            document.getElementById('nav-close-btn').addEventListener('click', app.toggleNav, false);

            // close nav by touching the partial off-screen content
            document.addEventListener('click', function(e)
            {
                if (nav_open && !hasParent(e.target, 'nav')) {
                    e.preventDefault();
                    app.closeNav();
                }
            },
            true);

            addClass(doc, 'js-ready');

        };

        return app;

    })();

    if (window.addEventListener) {
        window.addEventListener('DOMContentLoaded', window.App.init, false);
    }

})(window, window.document);

// main page script
// ====================================================
// $(document).ready(function() 
// {
//     var options =
//     {
//         dropDownSpeed : 100,
//         slideUpSpeed : 200,
//         slideDownTabSpeed: 50,
//         changeTabSpeed: 200
//     }
    
//     var methods = 
//     {
//         dropDownMenu : function(e)
//         {  
//             var body = $(this).find('> :last-child');
//             var head = $(this).find('> :first-child');
            
//             if (e.type == 'mouseover')
//             {
//                 body.fadeIn(options.dropDownSpeed);
//                 head
//             }
//             else
//             {
//                 body.fadeOut(options.dropDownSpeed);
//             }
            
//         },
        
//         dropDownClick : function(e)
//         {	
//         	if (!$(this).hasClass('default-click')){
// 				e.preventDefault();
// 			}
//             var dropdown = $(this).parents('.dropdown');
//             var selected = dropdown.find('.dropmenu .selected');
//             var newSelect = $(this).html();
//             var body = dropdown.find('> :last-child');
            
//             dropdown.find('.drop-selected').removeClass('drop-selected');
//             $(this).addClass('drop-selected');
//             selected.html(newSelect);
//             body.fadeOut(options.dropDownSpeed);	
            
//         },
        
//         //param slideFrom - side from where overlay will slide, target - parent element, that contains overlay
//         overlayHover : function(e)
//         {
//             var overlay;
//             var slideFrom = e.data.slideFrom;
//             var content;
            
//             if (typeof e.data.target == 'string')
//             {   
//                 overlay = $(this).parents(e.data.target).find('.overlay-wrp');
//                 content = $(this).parents(e.data.target).find('.content-wrp');
//             }
//             else
//             {
//                 overlay = $(this).find('.overlay-wrp');
//                 content = $(this).find('.content-wrp');
//             }
            
//             if ( (e.type == 'mouseover' && !overlay.hasClass('animating')) ||
//                  (e.type == 'click' && !overlay.hasClass('animating') && !overlay.hasClass('active')) )
//             {
//                 var animation = {};
//                 animation[slideFrom] = '0%';
//                 overlay.addClass('animating').addClass('active');
//                 overlay.stop().animate(animation, options.slideUpSpeed, function(){
//                     overlay.removeClass('animating').addClass('active');
//                 });
                
//                 animation[slideFrom] = '100%';
//                 content.parent().css('height', content.parent().height());
//                 content.stop(true,true).animate(animation, options.slideUpSpeed);
//             }
//             else if (e.type == 'mouseleave' || (e.type == 'click' && !overlay.hasClass('animating')))
//             {
//                 var animation = {};
//                 var animationContent = {};
                
//                 animationContent[slideFrom] = '0%';
//                 content.parent().css('height', content.parent().height());
//                 content.stop(true,true).animate(animationContent, options.slideUpSpeed, function()
//                 {
//                     content.parent().css('height', '');
//                 });
//                 animation[slideFrom] = '-100%';
//                 overlay.removeClass('active');
//                 overlay.stop(true,true).animate(animation, options.slideUpSpeed);
//             }
//         }
//     }

//     $('.profile-wrp .profile-photo, .profile-wrp .overlay-wrp').on('click', {slideFrom : 'left', target : '.hover-left'}, methods.overlayHover);
//     $('.hover-down').on('mouseover', {slideFrom : 'bottom'}, methods.overlayHover).on('mouseleave', {slideFrom : 'bottom'}, methods.overlayHover);
//     $('.dropdown').on('mouseover', methods.dropDownMenu).on('mouseleave', methods.dropDownMenu);
//     $('.dropdown .dropmenu-active a').on('click', methods.dropDownClick);
$(document).ready(function() 
{
    var options =
    {
        dropDownSpeed : 100,
        slideUpSpeed : 350,
        slideDownTabSpeed: 50,
        changeTabSpeed: 200
    }
    
    var methods = 
    {
        dropDownMenu : function(e)
        {  
            var body = $(this).find('> :last-child');
            var head = $(this).find('> :first-child');
            
            if (e.type == 'mouseover')
            {
                body.fadeIn(options.dropDownSpeed);
                head
            }
            else
            {
                body.fadeOut(options.dropDownSpeed);
            }
            
        },
        
        dropDownClick : function(e)
        {	
        	if (!$(this).hasClass('default-click')){
				e.preventDefault();
			}
            var dropdown = $(this).parents('.dropdown');
            var selected = dropdown.find('.dropmenu .selected');
            var newSelect = $(this).html();
            var body = dropdown.find('> :last-child');
            
            dropdown.find('.drop-selected').removeClass('drop-selected');
            $(this).addClass('drop-selected');
            selected.html(newSelect);
            body.fadeOut(options.dropDownSpeed);	
            
        },
        
        //param slideFrom - side from where overlay will slide, target - parent element, that contains overlay
        overlayHover : function(e)
        {
            var overlay;
            var slideFrom = e.data.slideFrom;
            var content;
            
            if (typeof e.data.target == 'string')
            {   
                overlay = $(this).parents(e.data.target).find('.overlay-wrp');
                content = $(this).parents(e.data.target).find('.content-wrp');
            }
            else
            {
                overlay = $(this).find('.overlay-wrp');
                content = $(this).find('.content-wrp');
            }
            
            if ( (e.type == 'mouseover' && !overlay.hasClass('animating')) ||
                 (e.type == 'click' && !overlay.hasClass('animating') && !overlay.hasClass('active')) )
            {
                var animation = {};
                animation[slideFrom] = '1';
                overlay.addClass('animating').addClass('active');
                overlay.stop().animate(animation, options.slideUpSpeed, function(){
                    overlay.removeClass('animating').addClass('active');
                });
                
                animation[slideFrom] = '0.2';
                content.parent().css('height', content.parent().height());
                content.stop(true,true).animate(animation, options.slideUpSpeed);
            }
            else if (e.type == 'mouseleave' || (e.type == 'click' && !overlay.hasClass('animating')))
            {
                var animation = {};
                var animationContent = {};
                
                animationContent[slideFrom] = '1';
                content.parent().css('height', content.parent().height());
                content.stop(true,true).animate(animationContent, options.slideUpSpeed, function()
                {
                    content.parent().css('height', '');
                });
                animation[slideFrom] = '0';
                overlay.removeClass('active');
                overlay.stop(true,true).animate(animation, options.slideUpSpeed);
            }
        }
    }

    $('.profile-wrp .profile-photo, .profile-wrp .overlay-wrp').on('click', {slideFrom : 'left', target : '.hover-left'}, methods.overlayHover);
    $('.hover-down').on('mouseover', {slideFrom : 'opacity'}, methods.overlayHover).on('mouseleave', {slideFrom : 'opacity'}, methods.overlayHover);
    $('.dropdown').on('mouseover', methods.dropDownMenu).on('mouseleave', methods.dropDownMenu);
    $('.dropdown .dropmenu-active a').on('click', methods.dropDownClick);



}); // End main script

// Flexslider
// ====================================================
$(window).load(function(){
	$('.flexslider').flexslider({
		animation: "slide",
		start: function(slider){
		  $('body').removeClass('loading');
		}
	});
});


//flickerfeed-setip.js http://www.pixiq.com/article/25-flickr
// ====================================================
$(document).ready(function(){

	$('#basicuse').jflickrfeed({
		limit: 12,
		qstrings: {
			id: '33506024@N02'
		},
		itemTemplate: '<li><a rel="photostream" class="fancybox" title="{{title}}" href="{{image_b}}"><i class="icon-search"></i><div class="hover"></div></a><img src="{{image_s}}" alt="{{title}}" /></li>'
	});

});

// Fancybox
// ====================================================
$(document).ready(function() {
	$(".fancybox").fancybox();
	$('.fancybox-media').fancybox({
		openEffect  : 'none',
		closeEffect : 'none',
		helpers : {
			media : {},
			title : {}
		}
	});
	$(".various").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
});

// hero carousel resize fix
// ====================================================
window.onresize = function(event){
    if ($('.hero-carousel').length > 0)
    {
        var carousel = $('.hero-carousel'),
            elements = carousel.children();
            childWidth = elements.width(),
    		childHeight = elements.height(),
            carouselWidth = Math.round(childWidth * elements.length),
            carouselMarginLeft = '-'+ Math.round(childWidth + Math.round(childWidth / 2) ) +'px',
            carouselPrevMarginLeft = parseFloat(carousel.css('margin-left'));
            
        $('.hero-carousel').css({
    		'left': '50%',
    		'width': carouselWidth,
    		'height': childHeight,
    		'margin-left': carouselMarginLeft
    	});
    }
}

// Github repos
// ====================================================
		$("[data-repo]").github();
		$(".github-box a").attr("target","_blank");

// Preload images
// ====================================================
$("body").waitForImages({waitForAll: !0,finished: function() {
        $("img").show()
    }});
