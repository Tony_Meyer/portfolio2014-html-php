// This is just a sample script. Paste your real code (javascript or HTML) here.
(function (a) {
    a.fn.extend({
        ulike: function (b) {
            var c = {
                icon: "heart",
                wording: "Like",
                path: "./ulike/",
                debug: false
            };
            var b = a.extend(c, b);
            return this.each(function () {
                obj = a(this);
                o = b;
                o.icon = o.icon.toLowerCase();
                obj.find("[ulike-data]").each(function (f, h) {
                    var j = a(this),
                        g = a(this).attr("ulike-data"),
                        e = window.location.href,
                        d = [];
                    d.push({
                        name: "uid",
                        value: g
                    }), d.push({
                        name: "page",
                        value: e
                    });
                    a.ajax({
                        url: o.path + "get_likes.php",
                        data: d,
                        type: "post",
                        success: function (k) {
                            var i = a.parseJSON(k),
                                m = i.count;
                            var l = a('<div class="ulike ulike-button"><ul class="ulike-layout"><li class=""><img src="' + o.path + "img/" + o.icon + '.png" class="icon"/> ' + o.wording + '</li><li class="ulike-count"><img src="' + o.path + 'img/arrow.png" class="arrow"/><span class="count"></span></li></ul></div>');
                            j.append(l.attr("ulike-data", g));
                            l.find("span.count").text(m)
                        }
                    })
                });
                a(".ulike-button").live("click", function (i) {
                    var j = a(this),
                        h = j.parents("div"),
                        g = h.attr("ulike-data"),
                        f = window.location.href,
                        d = [];
                    d.push({
                        name: "uid",
                        value: g
                    }), d.push({
                        name: "page",
                        value: f
                    });
                    a.ajax({
                        url: o.path + "like.php",
                        data: d,
                        type: "post",
                        success: function (k) {
                            var e = a.parseJSON(k);
                            if (!e.error) {
                                j.next("li").find(".count").text(e.count)
                            }
                            if (o.debug) {
                                alert(e.message)
                            }
                        }
                    });
                    i.preventDefault()
                })
            })
        }
    })
})(jQuery);