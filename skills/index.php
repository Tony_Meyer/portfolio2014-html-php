<?php 
    require_once("../_config.php");  
    require_once(PARTS . "/_header.php");  
?>
	<section class="pTop120"></section>
<style type="text/css">
.canvas-container {
    min-height: 250px;
    max-height: 250px;
    position: relative;
}
.widget {
    position: relative;
    margin-bottom: 80px;
    padding: 12px;
    margin-bottom: 30px;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.widget p {
    margin-top: 0;
    text-align: center;
}
.widget h3 {
    margin: -12px 0 12px -12px;
    padding: 12px;
    width: 100%;
    text-align: center;
    color: #627b86;
    line-height: 2em;
    font-size: 24px;
}


/* Doughnut-specific */
.widget.doughnut .canvas-container {
    min-height: 250px;
    max-height: 250px;
}
.widget.doughnut .status {
    display: block;
    position: absolute;
    top: 50%;
    left: 50%;
    width: 30%;
    height: 0;
    padding-top: 12%;
    padding-bottom: 18%;
    color: #444;
    margin-top: -15%;
    margin-left: -15%;
    font-size: 1.4em;
    font-weight: 700;
    text-align: center;
    border-radius: 50%;
   	background-repeat: no-repeat;
    background-size: 30%;
    background-position: center;
}
/* Line-specific */
.widget.line p span {
    color: #dbba34;
}
.widget.line p strong {
    color: #637b85;
    font-weight: 400;
}



.chart-legend ul {
    list-style: none;
    width: 100%;
    margin: 30px auto 0;
}
.chart-legend li {
    text-indent: 16px;
    line-height: 24px;
    position: relative;
    font-weight: 200;
    display: block;
    float: left;
    width: 50%;
    font-size: 0.8em;
}
.chart-legend  li:before {
    display: block;
    width: 10px;
    height: 16px;
    position: absolute;
    left: 0;
    top: 3px;
    content: "";
}
.css:before { background-color: #637b85; }
.js:before { background-color: #2c9c69; }
.flash:before { background-color: #dbba34; }
.php:before { background-color: #c62f29; }
.ps:before { background-color: #25A9E7; }
.ai:before { background-color: #C029C6; }

</style>
	<!-- Start Title -->
	<section id="title" class="container">
		<div class="row">
			<div class="span12">
				<h1 style="margin-bottom:0;">Skills</h1>
				<p>Things I can do</p>
			</div>
		</div>
	</section>
	<!-- End Title -->
			<!-- Start Intro Block -->
	<section class="intro container">
		<div class="row">
			<div class="span12">
								<p>Currently I am focusing on improving my development workflow, efficiency and skill base with: <span class="color-text">Angular JS, Node JS, Express JS </span>(that's a lot of JS !?!!) and a variety of tools and task runners like Grunt JS</p>
			</div>
		</div>
	</section>
<div class="container">
		
			<div class="content">
			<div class="row">
			<div class="span8">
				<div class="row">
					<div class="span8">
						<h5 class="cv-section-title padBot">Web Development & Design Skills</h5>
					</div>
				</div>			
					<div class="row">
						<div class="span8 skill">
							<h4>HTML & CSS </h4>

							<div class="level">
								<span class="l-8">extensive knowledge</span>
							</div>

							<p>extensive knowledge</p>
						</div>

						<div class="span8 skill">
							<h4>JavaScript</h4>

							<div class="level">
								<span class="l-8">extensive knowledge</span>
							</div>

							<p>extensive knowledge</p>
						</div>
						
						<div class="span8 skill">
							<h4>Adobe Flash</h4>

							<div class="level">
								<span class="l-8">extensive knowledge</span>
							</div>

							<p>extensive knowledge</p>
						</div>

						<div class="span8 skill">
							<h4>PHP</h4>

							<div class="level">
								<span class="l-7">advanced skill set</span>
							</div>

							<p>advanced skill set</p>
						</div>

						<div class="span8 skill">
							<h4>Adobe Photoshop</h4>

							<div class="level">
								<span class="l-6">good skill set</span>
							</div>

							<p>good skill set</p>
						</div>
						
						<div class="span8 skill">
							<h4>Adobe Indesign & Illustrator</h4>

							<div class="level">
								<span class="l-4">average skill set</span>
							</div>

							<p>average skill set</p>
						</div>
					</div>

						<!--languanges-->	
					<div class="row">
						<div class="span8">
							<h5 class="cv-section-title padBot">Language Skills</h5>
						</div>
					</div>

					<div class="row">
						<div class="span8 skill">
							<h4>English</h4>

							<div class="level">
								<span class="l-10">native speaker</span>
							</div>

							<p>native speaker</p>
						</div>

						<div class="span8 skill">
							<h4>German</h4>

							<div class="level">
								<span class="l-10">native speaker</span>
							</div>

							<p>native speaker</p>
						</div>

						<div class="span8 skill">
							<h4>French</h4>

							<div class="level">
								<span class="l-6">fluent</span>
							</div>

							<p>fluent</p>
						</div>
					</div>
				</div>
				<div class="span4">
					<h5 class="cv-section-title padBot"></h5>
				</div>
				<!--chart js-->
				<div class="span4">
		      <div class="third widget doughnut">
              <div class="canvas-container">
                  <canvas id="hours"></canvas>
                  <span class="status"></span>
              </div>
              <div class="chart-legend">
                  <ul>
                      <li class="css">HTML5 & CSS3</li>
                      <li class="js">JavaScript</li>
                      <li class="flash">FLASH</li>
                      <li class="php">PHP</li>
                      <li class="ps">Photoshop</li>
                      <li class="ai">Illustrator & InDesign</li>
                  </ul>
              </div>
          </div>
				</div>
				
				<div class="span4">
					<h5 class="cv-section-title padBot"></h5>
				</div>
			</div><!--end first row-->
		</div>
	</div>
	<?php require_once(PARTS . "/_footer.php");  ?>
