<?php


$uid = $_POST['uid'];
$page = $_POST['page'];
$ip = $_SERVER['REMOTE_ADDR'];

$error = false;

//Let's check to see if we have the proper variables coming in from the ajax post request
//If not lets load and array with an error and a message

if(!$uid)
{
	$json = array("error"=>true, "message"=>"Error: Could not find uid");
	$error = true;
}else if(!$page)
{
	$json = array("error"=>true, "message"=>"Error: Could not find page");
	$error = true;
}else if(!$ip)
{
	$json = array("error"=>true, "message"=>"Error: Could not find ip");
	$error = true;
}

//If we do not have an error, lets go ahead and start cleaning the variables
if(!$error)
{
	//Include your mysql connection file
	include 'mysql_conf.php';
	
	$ip = mysql_real_escape_string( $ip );
	$page = mysql_real_escape_string( $page );
	$uid = mysql_real_escape_string( $uid );
	
	//Lets see if the user / ip address has already liked the article on this page
	
	$query = mysql_query("SELECT u_id FROM tbllikes WHERE u_ip = '".$ip."' AND u_page = '".$page."' AND u_uid = '".$uid."' LIMIT 1");
	$count = mysql_num_rows( $query );
	
	//If the user hasn't then let's go ahead and add the like to the likes table
	//If there is an error lets output an error message, else lets go ahead and output a success message
	if(!$count)
	{
		$query = mysql_query("SELECT COUNT(u_id) AS u_count FROM tbllikes WHERE u_page = '".$page."' AND u_uid = '".$uid."' LIMIT 1");
		$row = mysql_fetch_assoc( $query );
		
		$count = $row['u_count'] + 1;// add one to the database count for this specific article
		
		if( mysql_query("INSERT INTO tbllikes VALUES('','".$uid."','".$page."','".$ip."',NULL)") )//add record to table
		{
			$json = array("error"=>false, "message"=>"Successfully liked", "count"=>$count);
		}else
		{
			$json = array("error"=>true, "message"=>"Error: Could not process like");
		}
	}else
	{
		//This means we already have a like for this article on this page
		//$json = array("error"=>true, "message"=>"Error: IP address already likes ".$uid." content from: ".$page);
		$json = array("error"=>true, "message"=>"You have already liked this.");
	}
}

//Kill all the processes and output the array in json format
die( json_encode( $json ) );