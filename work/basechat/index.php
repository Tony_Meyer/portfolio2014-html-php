<?php 
    require_once("../../_config.php");  
    require_once(PARTS . "/_header.php");  
?>
  <section class="pTop120"></section>
  <!-- End Header -->

<!-- Start Project Section -->
<article id="project">
  <div class="background">
        <img src="../../samsonite/uploads/portfolio-bg.jpg" class="attachment-showcase wp-post-image" alt="portfolio-bg" />     
    <div class="pattern"></div>
    <div class="gradient"></div>
  </div>
  <div class="container"><!-- Begin Case -->
  <!-- Start Project Description -->
  
      <div class="container">
    
      <!-- Start Project Description -->
      <div class="row">
        <div class="span8">
          <h1 class="color-text">Basechat</h1>
        </div>
      </div>
      <div class="row">
        <div class="span8">
          <p class="project-info">Client: me<span class="project-mounth">Date: October, 2013</span></p>
        </div>
        <div class="span4">
          <a class="launch" href="http://basechat.herokuapp.com/" target="_blank"><i class="icon-link"></i><span>Launch application</span></a>
        </div>
      </div>
      <div class="row"><div class="span12 line"></div></div>
      <!-- End Project Description -->
      <!-- Start Case -->
      <section id="case">
        <div class="row">
          <div class="span8">
            <img class="project-img" src="../../samsonite/themes/portfolio2014/img/portfolio/basechat/basechat-home.jpg" alt="basechat website" />
          </div>
          <div class="span4 image-desc" ulike-data="basechat">
            <h5>Basechat</h5>
            <p>This application is built with Angular JS, it uses Firebase as a real time backend DB, and Node JS for several development conveniences. </p> 
            <h5>Implementation</h5>
            <p>This is an experimentation and learning project, the development, programming, and deploying of the application was managed by me.</p>
            <h5>Repository</h5>                                                
            <div data-repo="thm-design/realtimechat" class="github-box-wrap project-single"></div>
            <br/>
            <!--a href="#" class="thm-likes" id="thm-likes-541" title="Like this"><span class="thm-likes-count">1</span> <span class="thm-likes-postfix"></span></a-->
          </div>
        </div>
      </section>
      <!-- End Case -->
    </div>  
  </div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
  <div class="row">
    <div class="span12">
      <a class="arrow pull-left" href="../portfolio2009/" title="Portfolio 2009"><i class="icon-chevron-left"></i>Previous</a>      <a class="arrow pull-right" href="../octopress/" title="Octopress">Next<i class="icon-chevron-right"></i></a>     <a class="back" href="../">Back to portfolio</a>
    </div>
  </div>
</section>
<!-- End Pagination -->
<?php require_once(PARTS . "/_footer.php");  ?>
<script>
//  var click = "5";
//  $('.thm-likes-count').text(click + ' times'); 
//  $('.thm-likes').click(function (e) { 
//     $(this).find('.thm-likes-count').text(click + ' times');
//     click ++;
//     e.preventDefault();

// });
// $('.thm-likes').click(function () { // click on button
//    $(this).prop('disabled', true); // set its disabled property to true
// }
</script>