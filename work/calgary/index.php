<?php 
    require_once("../../_config.php");  
    require_once(PARTS . "/_header.php");  
?>
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project">
	<div class="background">
				<img src="../../samsonite/uploads/portfolio-bg.jpg" class="attachment-showcase wp-post-image" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">CalagryWeddingPix</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: Calagary Wedding Photography<span class="project-mounth">Date: December, 2012</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="#" target=""><i class="icon-link"></i><span>Launching soon</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="../../samsonite/themes/portfolio2014/img/portfolio/calgary/calgary_detail.jpg" alt="CalgaryWeddingPix website" />
					</div>
					<div class="span4 image-desc" ulike-data="calgary">
						<h5>Calgary Wedding Pix</h5>
<p>A simple and clean responsive website for a Calagry based photography company. WordPress was chosen as a CMS here for it&#8217;s easy to use of content management system and fast setup.</p>
						<p>Professional photographers, photography mentors and photography business coaching, the kind people at CalgaryWeddingPix are dedicated to providing the highest quality photography for any event, creating magical ambiences that will make you feel like you are in a fary tale.</p>
					</div>
				</div>
				<div class="row">
					<div class="span8">
						<!-- Start Flex Slider -->
						<div class="flexslider">
							<ul class="slides">
								<li>
									<img src="../../samsonite/themes/portfolio2014/img/portfolio/calgary/calgary-slider4.jpg" alt="slider image 1" />
								</li>
								<li>
									<img src="../../samsonite/themes/portfolio2014/img/portfolio/calgary/calgary-slider2.jpg" alt="" />
								</li>
								<li>
									<img src="../../samsonite/themes/portfolio2014/img/portfolio/calgary/calgary-slider1.jpg" alt="" />
								</li>
								<li>
									<img src="../../samsonite/themes/portfolio2014/img/portfolio/calgary/calgary-slider3.jpg" alt="" />
								</li>
							</ul>
						</div>
						<!-- End Flex Slider -->
					</div>
					<div class="span4 image-desc">
						<h5>Features</h5>
						<p>
							<ul>
								<li>Responsive design</li>
								<li>Image gallery</li>
								<li>Multiple page layouts</li>
								<li>Pricing Table</li> 
								<li>CMS integration</li>
							</ul>
						</p>
					</div>
				</div>
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="../pingzilla/" title="pingzilla"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="../edoc/" title="edoc">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="../">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
	
<?php require_once(PARTS . "/_footer.php");  ?>