<?php 
    require_once("../../_config.php");  
    require_once(PARTS . "/_header.php");  
?>
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project">
	<div class="background">
				<img src="../../samsonite/uploads/portfolio-bg.jpg" class="attachment-showcase wp-post-image" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">Design4lifeblog</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: Me<span class="project-mounth">Date: November, 2011</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="#" target="_blank"><i class="icon-link"></i><span>Not online</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<div class="row project-description">
				<div class="span8">
					<p>Update: As of November 2013 I have taken this website down, I have come along way since then, I may eventually run some sort of blog again or something similar in the near future. This experiment is thus concluded for now :) </p>
				</div>
			</div>
			<!-- End Project Description -->
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="../../samsonite/themes/portfolio2014/img/portfolio/d4l/d4l_detail1.jpg" alt="design4lifeblog" />
						<img class="project-img" src="../../samsonite/themes/portfolio2014/img/portfolio/d4l/d4l_detail2.jpg" alt="design4lifeblog" />
					</div>
					<div class="span4 image-desc" ulike-data="d4l">
						<h5>About Design4lifeblog</h5>
            <p>I wanted to jump on the blog bandwagon, get to know wordpress better and see if I could create some passive income. This is the result.</p>
						<p>Design4life provides information on current web technologies, web design and development resources, news, freebies and tutorials.</p>
					</div>
				</div>
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="../edoc/" title="edoc"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="../valovis/" title="Valovis Bank">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="../">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
	<?php require_once(PARTS . "/_footer.php");  ?>