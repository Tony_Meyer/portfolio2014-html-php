<?php 
    require_once("../../_config.php");  
    require_once(PARTS . "/_header.php");  
?>
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project">
	<div class="background">
				<img src="../../samsonite/uploads/portfolio-bg.jpg" class="attachment-showcase wp-post-image" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">eDoc solutions</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: eDoc solutions AG<span class="project-mounth">Date: September, 2011</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="http://www.edoc.de/" target="_blank"><i class="icon-link"></i><span>Launch website</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>	
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="../../samsonite/themes/portfolio2014/img/portfolio/edoc/edocdetail_1.jpg" alt="eDoc solutions AG website" />
						<img class="project-img" src="../../samsonite/themes/portfolio2014/img/portfolio/edoc/edocdetail_2.jpg" alt="eDoc solutions AG website" />
					</div>
					<div class="span4 image-desc" ulike-data="edoc">
						<h5>edoc.de</h5>
						<p>I was hired by BLUEMARS in Frankfurt to code up their finished design for eDoc Solutions and create an HTML template and assist in the integration of the site into Typo3 CMS. Technolgies used to create it: HTML, PHP, JavaScript and Typo3.</p>
						<p>edoc solutions ag are a specialist for services relating to the management of enterprise-wide information. With our offer of a hand of hardware and software to support, we stand out consistently on many other medium-sized suppliers. Compared to large companies, which have a similar range of performance, we are more personal, more flexible, more efficient.				
							<a href="http://www.edoc.de/" target="_blank">more info..</a></p>
						<h5>Concept &#038; Design</h5>
						<p>Concept and Design by the talented people at www.bluemars.net</p>
						<h5>Technical Implementation</h5>
						<p>The development, programming and building of the site was managed by myself, while the lead dev @ Bluemars handled the bulk of Typo3 integration.</p>
					</div>
				</div>
				
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="../calgary/" title="calgary"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="../design4lifeblog/" title="Design4lifeblog">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="../">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
<?php require_once(PARTS . "/_footer.php");  ?>