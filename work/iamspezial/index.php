<?php 
    require_once("../../_config.php");  
    require_once(PARTS . "/_header.php");  
?>
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project">
	<div class="background">
				<img src="../../samsonite/uploads/portfolio-bg.jpg" class="attachment-showcase wp-post-image" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">MERZ SPEZIAL</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: Merz Phamaceuticals GmbH<span class="project-mounth">Date: September, 2011</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="http://www.merz-spezial.com/" target="_blank"><i class="icon-link"></i><span>Launch website</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="../../samsonite/themes/portfolio2014/img/portfolio/merz/iamspezial-home.jpg" alt="iamspezial website" />
					</div>
					<div class="span4 image-desc" ulike-data="iamspezial">
						<h5>I AM Spezial Campaign</h5>
<p>While working at GPM mbH in Frankfurt I had the opportunity to be a part of the team that created this website. Web technolgies used to create it: HTML, PHP, JavaScript. In a sprint phase it was integrated into FirstSpirit CMS.</p>
						<p>Merz Pharma GmbH &#038; Co. KGaA is an international healthcare company specializing in the research, development and marketing of pharmaceuticals for the treatment of neurological and psychiatric diseases. Based in Frankfurt am Main, Germany, Merz is a leader in the field of Alzheimer&#8217;s disease research and developed memantine as the first drug for the treatment of moderate to severe Alzheimer&#8217;s disease. Other franchises covered by Merz are hepatology / metabolic diseases and dermatology (including Mederma scar gel). In Asia Pacific, the Merz franchise covers aesthetic dermatology (including Radiesse and Xeomin). Merz was founded in 1908. 
						<a href="http://www.merz.com/" target="_blank">more info..</a></p>
						<h5>Concept &#038; Design</h5>
						<p>Concept and Design was done by the talented people at www.leoburnett.de</p>
						<h5>Technical Implementation</h5>
						<p>The development, programming and building of the site was managed by the creative agency www.gpm-agentur.net</p>
					</div>
				</div>
				<div class="row">
					<div class="span8">
						<!-- Start Flex Slider -->
						<div class="flexslider">
							<ul class="slides">
								<li>
									<img src="../../samsonite/themes/portfolio2014/img/portfolio/merz/iamspezial-slider1.jpg" alt="slider image 1" />
								</li>
								<li>
									<img src="../../samsonite/themes/portfolio2014/img/portfolio/merz/iamspezial-slider2.jpg" alt="" />
								</li>
								<li>
									<img src="../../samsonite/themes/portfolio2014/img/portfolio/merz/iamspezial-slider3.jpg" alt="" />
								</li>
							</ul>
						</div>
						<!-- End Flex Slider -->
					</div>
					<div class="span4 image-desc">
						<h5>Features</h5>
						<p>
							<ul>
								<li>Video/photo grid</li>
								<li>Background image gallery</li>
								<li>Multiple page layouts</li>
								<li>Live JavaScript validation</li> 
								<li>CMS integration</li>
							</ul>
						</p>
					</div>
				</div>
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
						<a class="arrow pull-right" href="../pingzilla/" title="pingzilla">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="../">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
	<?php require_once(PARTS . "/_footer.php");  ?>