<?php 
    require_once("../_config.php");  
    require_once(PARTS . "/_header.php");  
?>
	<section class="pTop120"></section>
	<!-- End Header -->	<!-- Start Title -->
	<section id="page-title" class="container">
		<div class="row">
			<div class="span10">
				<h2>Portfolio</h2>
				<div class="dropdown">
			    	<div class="dropmenu">
				    	<p class="menu-title">Show</p>
				    	<p class="selected">All Projects</p>
				    	<i class="icon-chevron-down"></i>
			    	</div>
			    	<div class="dropmenu-active">
				    	<ul class="option-set" data-option-key="filter">
				    		<li><a class="selected drop-selected" href="#filter" data-option-value="*">All Projects</a></li>
					    	<!-- <li class="active"><a href="javascript:void(0)" class="all">All</a></li> -->
					
							<li><a href="#filter" data-option-value=".coding">CODING</a></li><li><a href="#filter" data-option-value=".experimental">EXPERIMENTAL</a></li><li><a href="#filter" data-option-value=".flash">FLASH</a></li><li><a href="#filter" data-option-value=".graphics">GRAPHICS</a></li><li><a href="#filter" data-option-value=".video">VIDEO</a></li><li><a href="#filter" data-option-value=".wordpress">WORDPRESS</a></li>				    	</ul>
			    	</div>
			    </div>
			</div>
			<div class="span2 hidden-phone">
				<p><span class="number">20					</span>projects</p>
			</div>
		</div>
		<div class="row">
			<hr class="span12 clear-bottom">
		</div>
	</section>
	<!-- End Title -->

	<!-- Start Projects -->
	<section id="projects" class="container">
		<div id="posts" class="isotope row">
		
											
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding experimental ">
						<a href="basechat/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/basechat.jpg" class="attachment-portfolio wp-post-image" alt="basechat" />									
		
								<p class="project-name">Basechat</p>
								<p class="project-desc">real time chat application</p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Basechat</p>
								<p class="project-desc">Angluarfire, Node js</p>
								<p class="project-date">October 2013</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding experimental ">
						<a href="octopress/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/octoThumb.jpg" class="attachment-portfolio wp-post-image" alt="octoThumb" />									
		
								<p class="project-name">Octopress Portfolio</p>
								<p class="project-desc">experiment</p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Octopress Portfolio</p>
								<p class="project-desc">Octopress  experiment</p>
								<p class="project-date">October 2013</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding experimental ">
						<a href="pingzilla/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/pingzilla.png" class="attachment-portfolio wp-post-image" alt="pingzilla" />									
		
								<p class="project-name">Pingzilla</p>
								<p class="project-desc">web application</p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Pingzilla</p>
								<p class="project-desc">PHP + Ajax + Cron</p>
								<p class="project-date">Aug 2013</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding ">
						<a href="pcan-gateway/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/gatewaythumb1.jpg" class="attachment-portfolio wp-post-image" alt="gatewaythumb" />									
		
								<p class="project-name">PCAN Gateway</p>
								<p class="project-desc">web application</p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">PCAN Gateway</p>
								<p class="project-desc">PHP, JS, CSS, HTML</p>
								<p class="project-date">June 2013</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding graphics ">
						<a href="iamspezial/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/thumb-iamspezial.jpg" class="attachment-portfolio wp-post-image" alt="thumb-iamspezial" />									
		
								<p class="project-name">Merz Spezial</p>
								<p class="project-desc">Campaign Website</p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Merz Spezial</p>
								<p class="project-desc">HTML PHP JS + FirstSpirit CMS </p>
								<p class="project-date">September 2011</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding ">
						<a href="fileserver/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/thumb-filenice.jpg" class="attachment-portfolio wp-post-image" alt="thumb-filenice" />									
		
								<p class="project-name">Online File Directory</p>
								<p class="project-desc">Web Application</p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Online File Directory</p>
								<p class="project-desc">PHP JS HTML</p>
								<p class="project-date">May 2012</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding graphics wordpress ">
						<a href="stopandgocoffee/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/stopandgo_thumb.jpg" class="attachment-portfolio wp-post-image" alt="stopandgo_thumb" />									
		
								<p class="project-name">Stop &#038; Go Coffee</p>
								<p class="project-desc">Branding </p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Stop &#038; Go Coffee</p>
								<p class="project-desc">HTML JS PHP + digital and print graphics</p>
								<p class="project-date">January 2011</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding ">
						<a href="valovis/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/valovis_thumb2.jpg" class="attachment-portfolio wp-post-image" alt="valovis_thumb2" />									
		
								<p class="project-name">Valovis Bank</p>
								<p class="project-desc">Corporate Website</p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Valovis Bank</p>
								<p class="project-desc">Flash component</p>
								<p class="project-date">October 2011</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding experimental graphics wordpress ">
						<a href="design4lifeblog/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/thumb-d4l.jpg" class="attachment-portfolio wp-post-image" alt="thumb-d4l" />									
		
								<p class="project-name">Design4lifeblog</p>
								<p class="project-desc">Web Development Blog</p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Design4lifeblog</p>
								<p class="project-desc">Content driven blog</p>
								<p class="project-date">November 2011</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding ">
						<a href="edoc/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/thumb-edoc.jpg" class="attachment-portfolio wp-post-image" alt="thumb-edoc" />									
		
								<p class="project-name">eDoc solutions</p>
								<p class="project-desc">Corporate Website</p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">eDoc solutions</p>
								<p class="project-desc">HTML JS  and PHP template</p>
								<p class="project-date">August 2012</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding flash ">
						<a href="portfolio2009/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/thumb-port2009.jpg" class="attachment-portfolio wp-post-image" alt="thumb-port2009" />									
		
								<p class="project-name">Portfolio 2009</p>
								<p class="project-desc">2009 version of my portfolio</p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Portfolio 2009</p>
								<p class="project-desc">my old portfolio</p>
								<p class="project-date">September 2009</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding graphics wordpress ">
						<a href="calgary/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/thumb-calgary.jpg" class="attachment-portfolio wp-post-image" alt="thumb-calgary" />									
		
								<p class="project-name">Calgary Wedding Pix</p>
								<p class="project-desc">Photography Website</p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Calgary Wedding Pix</p>
								<p class="project-desc">HTML JS PHP + Wordpress</p>
								<p class="project-date">September 2012</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding experimental ">
						<a href="portfolio2011/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/thumb.jpg" class="attachment-portfolio wp-post-image" alt="thumb" />									
		
								<p class="project-name">Portfolio 2011</p>
								<p class="project-desc">2011 version of my portfolio </p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Portfolio 2011</p>
								<p class="project-desc">My old Portfolio</p>
								<p class="project-date">2011</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding wordpress ">
						<a href="portfolio2012/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/thumb1.jpg" class="attachment-portfolio wp-post-image" alt="thumb" />									
		
								<p class="project-name">Portfolio 2012</p>
								<p class="project-desc">2012 version of my portfolio </p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Portfolio 2012</p>
								<p class="project-desc">My old Portfolio</p>
								<p class="project-date">September 2012</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding wordpress ">
						<a href="portfolio-2013/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/portfolio2013_thumb1.jpg" class="attachment-portfolio wp-post-image" alt="portfolio2013_thumb" />									
		
								<p class="project-name">Portfolio 2013</p>
								<p class="project-desc">2013 version of my portfolio</p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Portfolio 2013</p>
								<p class="project-desc">Current Portfolio</p>
								<p class="project-date">September 2013</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding ">
						<a href="hyal/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/thumb2.jpg" class="attachment-portfolio wp-post-image" alt="thumb" />									
		
								<p class="project-name">Hyal System</p>
								<p class="project-desc">Product Website</p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Hyal System</p>
								<p class="project-desc">PHP, HTML, CSS</p>
								<p class="project-date">December 2012</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding flash ">
						<a href="virtualrooms/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/thumb3.jpg" class="attachment-portfolio wp-post-image" alt="thumb" />									
		
								<p class="project-name">Virtual Rooms</p>
								<p class="project-desc">Project Showcase</p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Virtual Rooms</p>
								<p class="project-desc">Flash ActionScript 3</p>
								<p class="project-date">May 2009</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item graphics ">
						<a href="planet49/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/thumb-49.jpg" class="attachment-portfolio wp-post-image" alt="thumb-49" />									
		
								<p class="project-name">Planet 49</p>
								<p class="project-desc">Photoshop Template</p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Planet 49</p>
								<p class="project-desc">Graphics for an ad campaign</p>
								<p class="project-date">January 2010</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item experimental video ">
						<a href="video/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/thumb-video.jpg" class="attachment-portfolio wp-post-image" alt="thumb-video" />									
		
								<p class="project-name">Videos</p>
								<p class="project-desc">Video and Mograph</p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Videos</p>
								<p class="project-desc">After Effects, Adobe Premier, Cinema4D</p>
								<p class="project-date">2009-2011</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
					
					<!-- Start Project -->
					<div class="post item span4 isotope-item coding ">
						<a href="memantine/" class="project-wrp hover-down" target="_self">

							<div class="content-wrp">
																<img src="../samsonite/uploads/thumb-mem.jpg" class="attachment-portfolio wp-post-image" alt="thumb-mem" />									
		
								<p class="project-name">Memantine</p>
								<p class="project-desc">Product Website</p>
							</div>
							<div class="overlay-wrp">
					        	<div class="overlay"></div>
								<p class="project-name">Memantine</p>
								<p class="project-desc">PHP, HTML, JavaScript</p>
								<p class="project-date">August 2012</p>
								<i class="icon-arrow-right"></i>
							</div>

						</a>		
					</div>
					<!-- End Project -->	
	
					
						
						
								
		</div>	
</section> 

<?php require_once(PARTS . "/_footer.php");  ?>