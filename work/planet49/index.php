<?php 
    require_once("../../_config.php");  
    require_once(PARTS . "/_header.php");  
?>
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project">
	<div class="background">
				<img src="../../samsonite/uploads/portfolio-bg.jpg" class="attachment-showcase wp-post-image" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">iPhone 5 Promo</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: Planet49 GmbH<span class="project-mounth">Date: Jan, 2010</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="#" target="_blank"><i class="icon-link"></i><span>Not online</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="../../samsonite/themes/portfolio2014/img/portfolio/p49/p49-home.jpg" alt="Portfolio 2013" />
						<img class="project-img" src="../../samsonite/themes/portfolio2014/img/portfolio/p49/p49-home2.jpg" alt="Portfolio 2013" />
<img class="project-img" src="../../samsonite/themes/portfolio2014/img/portfolio/p49/p49-home3.jpg" alt="Portfolio 2013" />
					</div>
					<div class="span4 image-desc" ulike-data="planet">
						<h5>Planet49</h5>
						<p>A PSD template created for an ad campaign for Plant49 GmbH.</p>
					</div>
				</div>
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="../video/" title="Videos"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="../memantine/" title="Memantine">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="../">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
<?php require_once(PARTS . "/_footer.php");  ?>