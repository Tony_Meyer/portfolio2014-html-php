<?php 
    require_once("../../_config.php");  
    require_once(PARTS . "/_header.php");  
?>
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project">
	<div class="background">
				<img src="../../samsonite/uploads/portfolio-bg.jpg" class="attachment-showcase wp-post-image" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">Portfolio 2011</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: me<span class="project-mounth">Date: September, 2011</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="../../../portfolio2011/" target="_blank"><i class="icon-link"></i><span>Launch website</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="../../samsonite/themes/portfolio2014/img/portfolio/portfolio2011/portfolio2011-home.jpg" alt="Portfolio 2011" />
					</div>
					<div class="span4 image-desc" ulike-data="p2011">
						<h5>Portfolio 2011</h5>
						<p>This is my personal portfolio website from 2011, I was still getting over the death of Flash. I soon made another portfolio site. Warning I do not maintain this site, full of broken links.</p>
					</div>
				</div>
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="../portfolio-2013/" title="Portfolio 2013"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="../portfolio2012/" title="Portfolio 2012">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="../">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
<?php require_once(PARTS . "/_footer.php");  ?>