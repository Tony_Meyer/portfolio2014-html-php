<?php 
    require_once("../../_config.php");  
    require_once(PARTS . "/_header.php");  
?>
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project">
	<div class="background">
				<img src="../../samsonite/uploads/portfolio-bg.jpg" class="attachment-showcase wp-post-image" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">Portfolio 2012</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: me<span class="project-mounth">Date: September, 2012</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="#" target="_blank"><i class="icon-link"></i><span>Not online</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="../../samsonite/themes/portfolio2014/img/portfolio/port2012.jpg" alt="portfolio 2012 website" />
						<img class="project-img" src="../../samsonite/themes/portfolio2014/img/portfolio/port20122.jpg" alt="portfolio 2012 website" />
					</div>
					<div class="span4 image-desc" ulike-data="p2012">
						<h5>Portfolio 2012</h5>
						<p>My portfolio from the year 2012, experimenting with WordPress here. With my move from Godaddy to a VPS(Digital Ocean) I have decided not to transfer over and update a few of my WordPress projects due to the process being too much of a hassle and time sink. This website is one of those projects and as a result is no longer online :(</p>
					</div>
				</div>
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="../portfolio2011/" title="Portfolio 2011"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="../hyal/" title="Hyal-System">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="../">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
<?php require_once(PARTS . "/_footer.php");  ?>