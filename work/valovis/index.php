<?php 
    require_once("../../_config.php");  
    require_once(PARTS . "/_header.php");  
?>
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project">
	<div class="background">
				<img src="../../samsonite/uploads/portfolio-bg.jpg" class="attachment-showcase wp-post-image" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">Valovis Bank</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Client: Valovis Bank AG<span class="project-mounth">Date: October, 2011</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="http://www.valovisbank.de/" target="_blank"><i class="icon-link"></i><span>Launch website</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<!-- End Project Description -->
			
			<!-- Start Case -->
			<section id="case">
				<div class="row">
					<div class="span8">
						<img class="project-img" src="../../samsonite/themes/portfolio2014/img/portfolio/valovis/valovis_detail1.jpg" alt="valovis bank" />
						<img class="project-img" src="../../samsonite/themes/portfolio2014/img/portfolio/valovis/valovis_slide1.png.jpeg" alt="valovis bank" />
					</div>
					<div class="span4 image-desc" ulike-data="valovis">
						<h5>Valovis Bank</h5>
<p>While working at GPM mbH in Frankfurt I was tasked with creating a complex flash component that would act as the homepage&#8217;s main navigation. Although using flash is uncommon these days there are still situations where it becomes a desired best solution. An image map was used as a fallback, so even IE 5.5 wasn't left out:)</p>
						<p>Valovis Bank is a company that specialises in trade and its interacting entities. Valovis bank connects four core business areas under one roof that make it sought after by financial experts and trade service providers in Germany. They offer private customers high quality products at attractive prices and allow customized solutions for corporate clients. The company, based in Essen and a second location in Neu-Isenburg employs approximately 250 employees and is a member of the Deposit Protection Fund of the Association of German Banks.
						<a href="http://valovisbank.de/" target="_blank">more info..</a></p>
						<h5>Concept &#038; Design</h5>
						<p>Concept and Design by the talented people at www.gpm-agentur.net</p>
						<h5>Technical Implementation</h5>
						<p>The development and programming of the flash based navigation was managed by myself.</p>
					</div>
				</div>
			</section>
			<!-- End Case -->
		</div>			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="../design4lifeblog/" title="Design4lifeblog"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="../fileserver/" title="File Server">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="../">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
<?php require_once(PARTS . "/_footer.php");  ?>