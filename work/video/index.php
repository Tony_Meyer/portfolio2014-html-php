<?php 
    require_once("../../_config.php");  
    require_once(PARTS . "/_header.php");  
?>
	<section class="pTop120"></section>
	<!-- End Header -->

<!-- Start Project Section -->
<article id="project">
	<div class="background">
				<img src="../../samsonite/uploads/portfolio-bg.jpg" class="attachment-showcase wp-post-image" alt="portfolio-bg" />			
		<div class="pattern"></div>
		<div class="gradient"></div>
	</div>
	<div class="container"><!-- Begin Case -->
	<!-- Start Project Description -->
	
			<div class="container">
		
			<!-- Start Project Description -->
			<div class="row">
				<div class="span8">
					<h1 class="color-text">Video</h1>
				</div>
			</div>
			<div class="row">
				<div class="span8">
					<p class="project-info">Clients: Various<span class="project-mounth">Date: 2009-2012</span></p>
				</div>
				<div class="span4">
					<a class="launch" href="http://vimeo.com/user2584000/videos" target="_blank"><i class="icon-link"></i><span>View on vimeo</span></a>
				</div>
			</div>
			<div class="row"><div class="span12 line"></div></div>
			<!-- Start Case -->
			<section id="case">
				<div class="row">
				<div class="span8">
					<!-- Start Responsive video -->
					<h4 class="margin">Experiment &#8211; The Rain</h4>
					<div class="embed-container">
						<iframe src="http://player.vimeo.com/video/19072025?byline=0&portrait=0&badge=0&color=b0b825" frameborder="0"></iframe>
					</div>
					<!-- End Responsive video -->
				</div>
					<div class="span4 image-desc" ulike-data="video">
						<h5>Videos and Experiments</h5>
						<p>Video production and motion graphics are not my primary focus at the moment. Fair warning, some of these videos were experiements, and some were done as part of my employment were I was wearing 4-5 different hats like: web designer/dev, marketing, print &#038; advertising, video production and post production etc. Needless to say these are not the works of a professional motion graphics artist. </p><p>There was a time where I was considering concentrating on becoming a professional in this area. But I fell in love with web development I learned a lot and know how to use to Adobe After Effects, Cinema 4d, Finalcut Pro and Adobe Premiere and know the basics of video production including lighting, equipment and post production.</p>
					</div>
				<div class="span8">
					<!-- Start Responsive video -->
					<h4 class="margin">Portfolio 2012 intro </h4>
					<div class="embed-container">
						<iframe src="http://player.vimeo.com/video/11045174?byline=0&portrait=0&badge=0&color=b0b825" frameborder="0"></iframe>
					</div>
					<!-- End Responsive video -->
				</div>
				<div class="span8">
					<!-- Start Responsive video -->
					<h4 class="margin">Portfolio work &#8211; Showreel Intro 2011</h4>
					<div class="embed-container">
						<iframe src="http://player.vimeo.com/video/18999065?byline=0&portrait=0&badge=0&color=b0b825" frameborder="0"></iframe>
					</div>
					<!-- End Responsive video -->
				</div>
				<div class="span8">
					<!-- Start Responsive video -->
					<h4 class="margin">Döhler animated logo concept pitch</h4>
					<div class="embed-container">
						<iframe src="http://player.vimeo.com/video/18753715?byline=0&portrait=0&badge=0&color=b0b825" frameborder="0"></iframe>
					</div>
					<!-- End Responsive video -->
				</div>
				
				<div class="span8">
					<!-- Start Responsive video -->
					<h4 class="margin">Tupperware Germany &#8211; Coolmax</h4>
					<div class="embed-container">
						<iframe src="http://player.vimeo.com/video/7435768?byline=0&portrait=0&badge=0&color=b0b825" frameborder="0"></iframe>
					</div>
					<!-- End Responsive video -->
				</div>
				<div class="span8">
					<!-- Start Responsive video -->
					<h4 class="margin">Tupperware Germany &#8211; Back Zauber</h4>
					<div class="embed-container">
						<iframe src="http://player.vimeo.com/video/19850187?byline=0&portrait=0&badge=0&color=b0b825" frameborder="0"></iframe>
					</div>
					<!-- End Responsive video -->
				</div>
				<div class="span8">
					<!-- Start Responsive video -->
					<h4 class="margin">Tupperware Germany &#8211; Logo animation</h4>
					<div class="embed-container">
						<iframe src="http://player.vimeo.com/video/7435526?byline=0&portrait=0&badge=0&color=b0b825" frameborder="0"></iframe>
					</div>
					<!-- End Responsive video -->
				</div>
				</div>
			</section>
			<!-- End Case -->
		</div>

			
	</div><!-- End Case -->
</article>
<!-- End Project Section -->
<!-- Start Pagination -->
<section class="left-right container">
	<div class="row">
		<div class="span12">
			<a class="arrow pull-left" href="../virtualrooms/" title="Virtual Rooms"><i class="icon-chevron-left"></i>Previous</a>			<a class="arrow pull-right" href="../planet49/" title="iPhone 5 Promo">Next<i class="icon-chevron-right"></i></a>			<a class="back" href="../">Back to portfolio</a>
		</div>
	</div>
</section>
<!-- End Pagination -->
	<?php require_once(PARTS . "/_footer.php");  ?>